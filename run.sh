#!/bin/bash
# Dev Server
#. venv/bin/activate && flask run --host 0.0.0.0 --port 8000
# Prod Server
. venv/bin/activate && gunicorn -b 0.0.0.0:8000 wsgi:app --workers=1 --threads=10 --timeout=1800
