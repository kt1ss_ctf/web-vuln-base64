from flask import Flask, render_template, request, abort
import platform
from subprocess import Popen, PIPE

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/encode', methods=['POST'])
def encode():
    if platform.system() != 'Linux':
        return 'Unfortunately only Linux is supported. Exiting...'

    plain_text = request.form.get('plain_text')

    if not plain_text:
        return abort(400)

    cmd = 'echo %s | base64 -' % plain_text
    p = Popen(cmd, shell=True, stdout=PIPE)
    data = p.communicate()
    data = data[0].decode().rstrip()

    return render_template('result.html', source=plain_text, result=data)

